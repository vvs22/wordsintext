package ua.hillel;

import ua.hillel.dataprovider.*;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        DataProvider dataProvider = new DataProvider();
        String nameOfFile = "src/ua/hillel/resources/text.txt";
        ArrayList<String> arrayListOfLines = new ArrayList<>();
        ArrayList<String> arrayListOfWords = new ArrayList<>();
        TreeMap<String, Integer> myMapOfWords = new TreeMap<>();
        System.out.println("Попробуем прочесть файл: " + nameOfFile);
        arrayListOfLines = dataProvider.readFile(nameOfFile);
        arrayListOfWords = dataProvider.extractWords(arrayListOfLines);
        for (String readWord : arrayListOfWords) {
            myMapOfWords = dataProvider.addWordToMap(myMapOfWords, readWord.toLowerCase());
        }
        for (HashMap.Entry<String, Integer> entry : myMapOfWords.entrySet()) {
            System.out.println(entry.getKey() + " = " + entry.getValue());
        }
    }
}
