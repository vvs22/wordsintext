package ua.hillel.dataprovider;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;

public class DataProvider {
    private String readLineFromStream;
    private ArrayList<String> arrayListOfLine = new ArrayList<>();
    private ArrayList<String> arrayListOfWords = new ArrayList<>();
    private TreeMap<String, Integer> myMapOfWords = new TreeMap<>();

    public TreeMap<String, Integer> addWordToMap(TreeMap<String, Integer> myMapOfWords, String nextWord) {
        if (myMapOfWords.containsKey(nextWord)) {
            myMapOfWords.put(nextWord, myMapOfWords.get(nextWord) + 1);
//            System.out.println(nextWord + " " + myMapOfWords.get(nextWord).toString());
        } else {
            myMapOfWords.put(nextWord, 1);
        }
        return myMapOfWords;
    }

    public ArrayList<String> readFile(String nameOfFile) {
        try (BufferedReader reader = new BufferedReader(new FileReader(nameOfFile))) {
            while ((readLineFromStream = reader.readLine()) != null) {
                arrayListOfLine.add(readLineFromStream);
            }
        } catch (IOException e) {
            System.out.println("Не могу найти указанный файл: " + nameOfFile);
        }
        return arrayListOfLine;
    }

    public ArrayList<String> extractWords(ArrayList<String> arrayListOfLine) {
        for (String lineFromFile : arrayListOfLine) {
            for (String nextWord : lineFromFile.split(" ")) {
//                Добавляю переменную - последний символ для уменьшения конструкции Если ...
                int i = nextWord.length();
                char lChr = nextWord.charAt(i - 1);
                if ((lChr <= 47 && lChr >= 32) || (lChr <= 64 && lChr >= 58) || (lChr <= 96 && lChr >= 91) || (lChr <= 126 && lChr >= 123)) {
                    nextWord = nextWord.substring(0, i - 1);
                }
                arrayListOfWords.add(nextWord);
            }
        }
        return arrayListOfWords;
    }
}
